import java.util.Locale;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Source {
	private static int[][] costs; // cost matrix
	private static int[][] weights; // weight matrix
	private static int[][] roots; // root matrix
	private static int[] q; // unsuccesful searches
	private static int[] p; // frequencies
	private static String[] keys;
	private static int numberOfkeys; // number of keys in the tree
	private static final String REGEX = "[^a-zA-Z]*([a-zA-Z]+)[^a-zA-Z]*";

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int z = scanner.nextInt();
		while (z-- > 0) {
			scanner.next();
			numberOfkeys = scanner.nextInt();
			costs = new int[numberOfkeys + 2][numberOfkeys + 2]; // cost matrix
			weights = new int[numberOfkeys + 1][numberOfkeys + 1]; // weight matrix
			roots = new int[numberOfkeys + 1][numberOfkeys + 1]; // root matrix
			q = new int[numberOfkeys + 1]; // unsuccesful searches
			p = new int[numberOfkeys + 1]; // frequencies
			keys = new String[numberOfkeys + 1];

			q[0] = scanner.nextInt();
			for (int i = 1; i <= numberOfkeys; i++) {
				Pattern pattern = Pattern.compile(REGEX);
				Matcher matcher = pattern.matcher(scanner.next());
				if (matcher.matches()) {
					keys[i] = matcher.group(1);
					p[i] = scanner.nextInt();
					q[i] = scanner.nextInt();
					scanner.nextLine();
				}
			}
			computeWCR();
			computeWCR();
			for (int i = 0; i < 100; i++) {
				computeWCR2();
			}
			display(constructOBST(0, numberOfkeys), 0);
			System.out.format(Locale.ENGLISH, "%.4f%n", ((float) costs[1][numberOfkeys] / weights[0][numberOfkeys]));//avg
		}
		scanner.close();
	}

	private static void computeWCR() {
		recount();
		for (int i = 0; i <= numberOfkeys; i++) {
			costs[i][i] = weights[i][i];
		}
		for (int i = 0; i <= numberOfkeys - 1; i++) {
			int j = i + 1;
			costs[i][j] = costs[i][i] + costs[j][j] + weights[i][j];
			roots[i][j] = j;
		}
		for (int h = 2; h <= numberOfkeys; h++) {
			for (int i = 0; i <= numberOfkeys - h; i++) {
				int j = i + h;
				int m = roots[i][j - 1];
				int min = costs[i][m - 1] + costs[m][j];
				for (int k = m + 1; k <= roots[i + 1][j]; k++) {
					int x = costs[i][k - 1] + costs[k][j];
					if (x < min) {
						m = k;
						min = x;
					}
				}
				costs[i][j] = weights[i][j] + min;
				roots[i][j] = m;
			}
		}
	}

	private static void recount() {
		for (int i = 0; i <= numberOfkeys; i++) {
			weights[i][i] = q[i];
			for (int j = i + 1; j <= numberOfkeys; j++) {
				weights[i][j] = weights[i][j - 1] + p[j] + q[j];
			}
		}
	}

	private static void computeWCR2() {
		recount();
		for (int i = 0; i < costs.length; i++) {
			costs[0][i] = 0;
		}
		for (int i = 1; i <= numberOfkeys; i++) {
			costs[i][i] = weights[i - 1][i];
		}
		for (int i = 1; i < costs.length - 1; i++) {
			for (int j = i; j < costs[0].length - 1; j++) {
				if (i != j) {
					costs[i][j] = weights[i - 1][j] + min(i, j);
				}
			}
		}
	}

	private static int min(int i, int j) {
		int min = Integer.MAX_VALUE;
		for (int x = 0; x < j - i + 1; x++) {
			int tmp = costs[i][i - 1 + x] + costs[i + 1 + x][j];
			if (min > tmp) {
				min = tmp;
			}
		}
		return min;
	}

	private static OptimalBinarySearchTreeNode constructOBST(int i, int j) {
		if (i == j) {
			return null;
		}
		OptimalBinarySearchTreeNode p = new OptimalBinarySearchTreeNode(keys[roots[i][j]]);
		p.left = constructOBST(i, roots[i][j] - 1);
		p.right = constructOBST(roots[i][j], j);
		return p;
	}

	private static void display(OptimalBinarySearchTreeNode root, int nivel) {
		if (root != null) {
			display(root.left, nivel + 1);
			for (int i = 0; i <= 2 * nivel; i++) {
				System.out.print(".");
			}
			System.out.println(root.key);
			display(root.right, nivel + 1);
		}
	}
}

class OptimalBinarySearchTreeNode {
	String key;
	OptimalBinarySearchTreeNode left;
	OptimalBinarySearchTreeNode right;

	OptimalBinarySearchTreeNode(String key) {
		this.key = key;
	}
}
